from PIL import Image
import os

def compress_image(input_path, output_path, max_size_kb, quality=85):
    # Check if the image is already below the maximum size
    if os.path.getsize(input_path) <= max_size_kb * 1024:
        img = Image.open(input_path)
        img.save(output_path)
        print(f"Image is already below {max_size_kb} KB, no compression needed.")
    else:
        img = Image.open(input_path)
        
        # Resize the image to Full HD (1920x1080)
        img = img.resize((1920, 1080), Image.ANTIALIAS)
        
        # Save the image with specified quality and format
        img.save(output_path, optimize=True, quality=quality)
        
        # Check if the compressed image is within the size limit
        while os.path.getsize(output_path) > max_size_kb * 1024:
            quality -= 5
            img.save(output_path, optimize=True, quality=quality)
        
        print(f"Compressed image saved at {output_path} with quality: {quality}")

# Provide input and output paths, maximum size in KB, and desired quality
input_image_path = "input_image.jpg"
output_image_path = "compressed_image.jpg"
max_size_kb = 50  # Maximum size in KB
desired_quality = 85  # Starting quality setting

compress_image(input_image_path, output_image_path, max_size_kb, desired_quality)
