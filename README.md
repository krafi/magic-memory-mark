Magic Memory Mark is a feature-rich in-memory text editor with magical markdown capabilities. It provides a user-friendly interface for editing text, applying various formatting options, and previewing markdown content as rendered HTML.

## keep note , 

**This program is specifically designed to be used as an embedded text editor within other applications where you want a text editor that saves data in memory rather than on disk for security and safety reasons. It also comes with the ability to start a local HTTP server to preview your markdown content directly in your web browser.**
# Prerequisites
- Python (3.6+) 
- PySide2
- markdown2 


`pip install PySide2 markdown2`
