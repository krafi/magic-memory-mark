import os
import hashlib
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

class FileEncryptor:

    def unpad(self, data):
        padding = data[-1]
        return data[:-padding]

    def pad(self, data, block_size):
        padding = block_size - len(data) % block_size
        return data + bytes([padding] * padding)

    def encrypt_var_data(self, data, password):
        block_size = 16
        key = hashlib.sha256(password.encode()).digest()
        iv = get_random_bytes(block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)

        padded_data = self.pad(data, block_size)
        encrypted_data = cipher.encrypt(padded_data)
        return b'ENCRYPTED:' + iv + encrypted_data

    def decrypt_var_data(self, encrypted_data, password):
        block_size = 16
        key = hashlib.sha256(password.encode()).digest()

        if not encrypted_data.startswith(b'ENCRYPTED:'):
            raise ValueError("The data is not encrypted with this program.")

        encrypted_data = encrypted_data[len(b'ENCRYPTED:'):]
        iv = encrypted_data[:block_size]
        ciphertext = encrypted_data[block_size:]
        cipher = AES.new(key, AES.MODE_CBC, iv)

        decrypted_data = self.unpad(cipher.decrypt(ciphertext))
        return decrypted_data


    def encrypt_and_save(self, file_path, password):
        with open(file_path, 'rb') as file:
            data = file.read()
            encrypted_data = self.encrypt_var_data(data, password)
            encrypted_path = file_path + '.enc'
            with open(encrypted_path, 'wb') as encrypted_file:
                encrypted_file.write(encrypted_data)
    def decrypt_and_save(self, encrypted_path, password):
        with open(encrypted_path, 'rb') as encrypted_file:
            encrypted_data = encrypted_file.read()
            decrypted_data = self.decrypt_var_data(encrypted_data, password)
            decrypted_path = encrypted_path.replace('.enc', '_decrypted')

            with open(decrypted_path, 'wb') as decrypted_file:
                decrypted_file.write(decrypted_data)  # Write binary data as-is

            print("File decrypted and saved as:", decrypted_path)

def main():
    encryptor = FileEncryptor()

    while True:
        print("1. Encrypt a file")
        print("2. Decrypt an encrypted file")
        print("3. Quit")
        choice = input("Enter your choice: ")

        if choice == '1':
            file_path = input("Enter the path of the file to encrypt: ")
            password = input("Enter the encryption password: ")
            encryptor.encrypt_and_save(file_path, password)
            print("File encrypted and saved.")

        elif choice == '2':
            encrypted_path = input("Enter the path of the encrypted file: ")
            password = input("Enter the decryption password: ")
            encryptor.decrypt_and_save(encrypted_path, password)
            print("File decrypted and saved.")

        elif choice == '3':
            print("Goodbye!")
            break

        else:
            print("Invalid choice. Please choose again.")

if __name__ == "__main__":
    main()
