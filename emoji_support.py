import sys
from PySide2 import QtWidgets, QtGui
from functools import partial

class EmojiPicker(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Emoji Picker")

        self.categories = {
                "Smileys": [
        "😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "🥲", "🥹",
        "☺️", "😊", "😇", "🙂", "🙃", "😉", "😌", "😍", "🥰", "😘",
        "😗", "😙", "😚", "😋", "😛", "😝", "😜", "🤪", "🤨", "🧐",
        "🤓", "😎", "🥸", "🤩", "🥳", "😏", "😒", "😞", "😔", "😟",
        "😕", "🙁", "☹️", "😣", "😖", "😫", "😩", "🥺", "😢", "😭",
        "😮‍💨", "😤", "😠", "😡", "🤬", "🤯", "😳", "🥵", "🥶",
        "😱", "😨", "😰", "😥", "😓", "🫣", "🤗", "🫡", "🤔", "🫢",
        "🤭", "🤫", "🤥", "😶", "😶‍🌫️", "😐", "😑", "😬", "🫨",
        "🫠", "🙄", "😯", "😦", "😧", "😮", "😲", "🥱", "😴", "🤤",
        "😪", "😵", "😵‍💫", "🫥", "🤐", "🥴", "🤢", "🤮", "🤧",
        "😷", "🤒", "🤕", "🤑", "🤠", "😈", "👿", "👹", "👺", "🤡",
        "💩", "👻", "💀", "☠️", "👽", "👾", "🤖", "🎃", "😺", "😸",
        "😹", "😻", "😼", "😽", "😿", "😾"
    ],
                "Gestures and Body Parts": [
        "👋", "🤚", "🖐", "✋", "🖖", "👌", "🤌", "🤏", "✌️", "🤞",
        "🫰", "🤟", "🤘", "🤙", "🫵", "🫱", "🫲", "🫸", "🫷", "🫳",
        "🫴", "👈", "👉", "👆", "🖕", "👇", "☝️", "👍", "👎", "✊",
        "👊", "🤛", "🤜", "👏", "🫶", "🙌", "👐", "🤲", "🤝", "🙏",
        "✍️", "💅", "🤳", "💪", "🦾", "🦵", "🦿", "🦶", "👣", "👂",
        "🦻", "👃", "🫀", "🫁", "🧠", "🦷", "🦴", "👀", "👁", "👅",
        "👄", "🫦", "💋", "🩸"
    ],
                    "People and Fantasy": [
        "👶", "👧", "🧒", "👦", "👩", "🧑", "👨", "👩‍🦱", "🧑‍🦱", "👨‍🦱",
        "👩‍🦰", "🧑‍🦰", "👨‍🦰", "👱‍♀️", "👱", "👱‍♂️", "👩‍🦳", "🧑‍🦳", "👨‍🦳",
        "👩‍🦲", "🧑‍🦲", "👨‍🦲", "🧔‍♀️", "🧔", "🧔‍♂️", "👵", "🧓", "👴", "👲",
        "👳‍♀️", "👳", "👳‍♂️", "🧕", "👮‍♀️", "👮", "👮‍♂️", "👷‍♀️", "👷", "👷‍♂️",
        "💂‍♀️", "💂", "💂‍♂️", "🕵️‍♀️", "🕵️", "🕵️‍♂️", "👩‍⚕️", "🧑‍⚕️", "👨‍⚕️",
        "👩‍🌾", "🧑‍🌾", "👨‍🌾", "👩‍🍳", "🧑‍🍳", "👨‍🍳", "👩‍🎓", "🧑‍🎓", "👨‍🎓",
        "👩‍🎤", "🧑‍🎤", "👨‍🎤", "👩‍🏫", "🧑‍🏫", "👨‍🏫", "👩‍🏭", "🧑‍🏭", "👨‍🏭",
        "👩‍💻", "🧑‍💻", "👨‍💻", "👩‍💼", "🧑‍💼", "👨‍💼", "👩‍🔧", "🧑‍🔧", "👨‍🔧",
        "👩‍🔬", "🧑‍🔬", "👨‍🔬", "👩‍🎨", "🧑‍🎨", "👨‍🎨", "👩‍🚒", "🧑‍🚒", "👨‍🚒",
        "👩‍✈️", "🧑‍✈️", "👨‍✈️", "👩‍🚀", "🧑‍🚀", "👨‍🚀", "👩‍⚖️", "🧑‍⚖️", "👨‍⚖️",
        "👰‍♀️", "👰", "👰‍♂️", "🤵‍♀️", "🤵", "🤵‍♂️", "👸", "🫅", "🤴", "🥷",
        "🦸‍♀️", "🦸", "🦸‍♂️", "🦹‍♀️", "🦹", "🦹‍♂️", "🤶", "🧑‍🎄", "🎅", "🧙‍♀️",
        "🧙", "🧙‍♂️", "🧝‍♀️", "🧝", "🧝‍♂️", "🧛‍♀️", "🧛", "🧛‍♂️", "🧟‍♀️", "🧟",
        "🧟‍♂️", "🧞‍♀️", "🧞", "🧞‍♂️", "🧜‍♀️", "🧜", "🧜‍♂️", "🧚‍♀️", "🧚", "🧚‍♂️",
        "🧌", "👼", "🤰", "🫄", "🫃", "🤱", "👩‍🍼", "🧑‍🍼", "👨‍🍼", "🙇‍♀️",
        "🙇", "🙇‍♂️", "💁‍♀️", "💁", "💁‍♂️", "🙅‍♀️", "🙅", "🙅‍♂️", "🙆‍♀️", "🙆",
        "🙆‍♂️", "🙋‍♀️", "🙋", "🙋‍♂️", "🧏‍♀️", "🧏", "🧏‍♂️", "🤦‍♀️", "🤦", "🤦‍♂️",
        "🤷‍♀️", "🤷", "🤷‍♂️", "🙎‍♀️", "🙎", "🙎‍♂️", "🙍‍♀️", "🙍", "🙍‍♂️", "💇‍♀️",
        "💇", "💇‍♂️", "💆‍♀️", "💆", "💆‍♂️", "🧖‍♀️", "🧖", "🧖‍♂️", "💅", "🤳",
        "💃", "🕺", "👯‍♀️", "👯", "👯‍♂️", "🕴", "👩‍🦽", "🧑‍🦽", "👨‍🦽", "👩‍🦼",
        "🧑‍🦼", "👨‍🦼", "🚶‍♀️", "🚶", "🚶‍♂️", "👩‍🦯", "🧑‍🦯", "👨‍🦯", "🧎‍♀️", "🧎",
        "🧎‍♂️", "🏃‍♀️", "🏃", "🏃‍♂️", "🧍‍♀️", "🧍", "🧍‍♂️", "👭", "🧑‍🤝‍🧑", "👬",
        "👫", "👩‍❤️‍👩", "💑", "👨‍❤️‍👨", "👩‍❤️‍👨", "👩‍❤️‍💋‍👩", "💏", "👨‍❤️‍💋‍👨", "👩‍❤️‍💋‍👨",
        "👪", "👨‍👩‍👦", "👨‍👩‍👧", "👨‍👩‍👧‍👦", "👨‍👩‍👦‍👦", "👨‍👩‍👧‍👧", "👨‍👨‍👦", "👨‍👨‍👧", "👨‍👨‍👧‍👦", "👨‍👨‍👦‍👦", "👨‍👨‍👧‍👧",
        "👩‍👩‍👦", "👩‍👩‍👧", "👩‍👩‍👧‍👦", "👩‍👩‍👦‍👦", "👩‍👩‍👧‍👧", "👨‍👦", "👨‍👦‍👦", "👨‍👧", "👨‍👧‍👦", "👨‍👧‍👧", "👩‍👦", "👩‍👦‍👦",
        "👩‍👧", "👩‍👧‍👦", "👩‍👧‍👧", "🗣", "👤", "👥", "🫂"
    ],

                        "Clothing and Accessories": [
        "🧳", "🌂", "☂️", "🧵", "🪡", "🪢", "🪭", "🧶", "👓", "🕶",
        "🥽", "🥼", "🦺", "👔", "👕", "👖", "🧣", "🧤", "🧥", "🧦",
        "👗", "👘", "🥻", "🩴", "🩱", "🩲", "🩳", "👙", "👚", "👛",
        "👜", "👝", "🎒", "👞", "👟", "🥾", "🥿", "👠", "👡", "🩰",
        "👢", "👑", "👒", "🎩", "🎓", "🧢", "⛑", "🪖", "💄", "💍",
        "💼"
    ],
                            "Pale Emojis": [
        "👋🏻", "🤚🏻", "🖐🏻", "✋🏻", "🖖🏻", "👌🏻", "🤌🏻", "🤏🏻", "✌🏻", "🤞🏻",
        "🫰🏻", "🤟🏻", "🤘🏻", "🤙🏻", "🫵🏻", "🫱🏻", "🫲🏻", "🫸🏻", "🫷🏻", "🫳🏻",
        "🫴🏻", "👈🏻", "👉🏻", "👆🏻", "🖕🏻", "👇🏻", "☝🏻", "👍🏻", "👎🏻", "✊🏻",
        "👊🏻", "🤛🏻", "🤜🏻", "👏🏻", "🫶🏻", "🙌🏻", "👐🏻", "🤲🏻", "🙏🏻", "✍🏻",
        "💅🏻", "🤳🏻", "💪🏻", "🦵🏻", "🦶🏻", "👂🏻", "🦻🏻", "👃🏻", "👶🏻", "👧🏻",
        "🧒🏻", "👦🏻", "👩🏻", "🧑🏻", "👨🏻", "👩🏻‍🦱", "🧑🏻‍🦱", "👨🏻‍🦱", "👩🏻‍🦰",
        "🧑🏻‍🦰", "👨🏻‍🦰", "👱🏻‍♀️", "👱🏻", "👱🏻‍♂️", "👩🏻‍🦳", "🧑🏻‍🦳", "👨🏻‍🦳",
        "👩🏻‍🦲", "🧑🏻‍🦲", "👨🏻‍🦲", "🧔🏻‍♀️", "🧔🏻", "🧔🏻‍♂️", "👵🏻", "🧓🏻", "👴🏻",
        "👲🏻", "👳🏻‍♀️", "👳🏻", "👳🏻‍♂️", "🧕🏻", "👮🏻‍♀️", "👮🏻", "👮🏻‍♂️", "👷🏻‍♀️",
        "👷🏻", "👷🏻‍♂️", "💂🏻‍♀️", "💂🏻", "💂🏻‍♂️", "🕵🏻‍♀️", "🕵🏻", "🕵🏻‍♂️", "👩🏻‍⚕️",
        "🧑🏻‍⚕️", "👨🏻‍⚕️", "👩🏻‍🌾", "🧑🏻‍🌾", "👨🏻‍🌾", "👩🏻‍🍳", "🧑🏻‍🍳", "👨🏻‍🍳",
        "👩🏻‍🎓", "🧑🏻‍🎓", "👨🏻‍🎓", "👩🏻‍🎤", "🧑🏻‍🎤", "👨🏻‍🎤", "👩🏻‍🏫", "🧑🏻‍🏫", "👨🏻‍🏫",
        "👩🏻‍🏭", "🧑🏻‍🏭", "👨🏻‍🏭", "👩🏻‍💻", "🧑🏻‍💻", "👨🏻‍💻", "👩🏻‍💼", "🧑🏻‍💼", "👨🏻‍💼",
        "👩🏻‍🔧", "🧑🏻‍🔧", "👨🏻‍🔧", "👩🏻‍🔬", "🧑🏻‍🔬", "👨🏻‍🔬", "👩🏻‍🎨", "🧑🏻‍🎨", "👨🏻‍🎨",
        "👩🏻‍🚒", "🧑🏻‍🚒", "👨🏻‍🚒", "👩🏻‍✈️", "🧑🏻‍✈️", "👨🏻‍✈️", "👩🏻‍🚀", "🧑🏻‍🚀", "👨🏻‍🚀",
        "👩🏻‍⚖️", "🧑🏻‍⚖️", "👨🏻‍⚖️", "👰🏻‍♀️", "👰🏻", "👰🏻‍♂️", "🤵🏻‍♀️", "🤵🏻", "🤵🏻‍♂️",
        "👸🏻", "🫅🏻", "🤴🏻", "🥷🏻", "🦸🏻‍♀️", "🦸🏻", "🦸🏻‍♂️", "🦹🏻‍♀️", "🦹🏻", "🦹🏻‍♂️",
        "🤶🏻", "🧑🏻‍🎄", "🎅🏻", "🧙🏻‍♀️", "🧙🏻", "🧙🏻‍♂️", "🧝🏻‍♀️", "🧝🏻", "🧝🏻‍♂️",
        "🧛🏻‍♀️", "🧛🏻", "🧛🏻‍♂️", "🧜🏻‍♀️", "🧜🏻", "🧜🏻‍♂️", "🧚🏻‍♀️", "🧚🏻", "🧚🏻‍♂️",
        "👼🏻", "🤰🏻", "🫄🏻", "🫃🏻", "🤱🏻", "👩🏻‍🍼", "🧑🏻‍🍼", "👨🏻‍🍼", "🙇🏻‍♀️", "🙇🏻",
        "🙇🏻‍♂️", "💁🏻‍♀️", "💁🏻", "💁🏻‍♂️", "🙅🏻‍♀️", "🙅🏻", "🙅🏻‍♂️", "🙆🏻‍♀️", "🙆🏻", "🙆🏻‍♂️",
        "🙋🏻‍♀️", "🙋🏻", "🙋🏻‍♂️", "🧏🏻‍♀️", "🧏🏻", "🧏🏻‍♂️", "🤦🏻‍♀️", "🤦🏻", "🤦🏻‍♂️",
        "🤷🏻‍♀️", "🤷🏻", "🤷🏻‍♂️", "🙎🏻‍♀️", "🙎🏻", "🙎🏻‍♂️", "🙍🏻‍♀️", "🙍🏻", "🙍🏻‍♂️",
        "💇🏻‍♀️", "💇🏻", "💇🏻‍♂️", "💆🏻‍♀️", "💆🏻", "💆🏻‍♂️", "🧖🏻‍♀️", "🧖🏻", "🧖🏻‍♂️",
        "💃🏻", "🕺🏻", "🕴🏻", "👩🏻‍🦽", "🧑🏻‍🦽", "👨🏻‍🦽", "👩🏻‍🦼", "🧑🏻‍🦼", "👨🏻‍🦼",
        "🚶🏻‍♀️", "🚶🏻", "🚶🏻‍♂️", "👩🏻‍🦯", "🧑🏻‍🦯", "👨🏻‍🦯", "🧎🏻‍♀️", "🧎🏻", "🧎🏻‍♂️",
        "🏃🏻‍♀️", "🏃🏻", "🏃🏻‍♂️", "🧍🏻‍♀️", "🧍🏻", "🧍🏻‍♂️", "👭🏻", "🧑🏻‍🤝‍🧑🏻", "👬🏻", "👫🏻",
        "🧗🏻‍♀️", "🧗🏻", "🧗🏻‍♂️", "🏇🏻", "🏂🏻", "🏌🏻‍♀️", "🏌🏻", "🏌🏻‍♂️", "🏄🏻‍♀️", "🏄🏻",
        "🏄🏻‍♂️", "🚣🏻‍♀️", "🚣🏻", "🚣🏻‍♂️", "🏊🏻‍♀️", "🏊🏻", "🏊🏻‍♂️", "⛹🏻‍♀️", "⛹🏻", "⛹🏻‍♂️",
        "🏋🏻‍♀️", "🏋🏻", "🏋🏻‍♂️", "🚴🏻‍♀️", "🚴🏻", "🚴🏻‍♂️", "🚵🏻‍♀️", "🚵🏻", "🚵🏻‍♂️",
        "🤸🏻‍♀️", "🤸🏻", "🤸🏻‍♂️", "🤽🏻‍♀️", "🤽🏻", "🤽🏻‍♂️", "🤾🏻‍♀️", "🤾🏻", "🤾🏻‍♂️",
        "🤹🏻‍♀️", "🤹🏻", "🤹🏻‍♂️", "🧘🏻‍♀️", "🧘🏻", "🧘🏻‍♂️", "🛀🏻", "🛌🏻"
    ],
                                "Cream White": [
        "👋🏼", "🤚🏼", "🖐🏼", "✋🏼", "🖖🏼", "👌🏼", "🤌🏼", "🤏🏼", "✌🏼", "🤞🏼",
        "🫰🏼", "🤟🏼", "🤘🏼", "🤙🏼", "🫵🏼", "🫱🏼", "🫲🏼", "🫸🏼", "🫷🏼", "🫳🏼",
        "🫴🏼", "👈🏼", "👉🏼", "👆🏼", "🖕🏼", "👇🏼", "☝🏼", "👍🏼", "👎🏼", "✊🏼",
        "👊🏼", "🤛🏼", "🤜🏼", "👏🏼", "🫶🏼", "🙌🏼", "👐🏼", "🤲🏼", "🙏🏼", "✍🏼",
        "💅🏼", "🤳🏼", "💪🏼", "🦵🏼", "🦶🏼", "👂🏼", "🦻🏼", "👃🏼", "👶🏼", "👧🏼",
        "🧒🏼", "👦🏼", "👩🏼", "🧑🏼", "👨🏼", "👩🏼‍🦱", "🧑🏼‍🦱", "👨🏼‍🦱", "👩🏼‍🦰",
        "🧑🏼‍🦰", "👨🏼‍🦰", "👱🏼‍♀️", "👱🏼", "👱🏼‍♂️", "👩🏼‍🦳", "🧑🏼‍🦳", "👨🏼‍🦳",
        "👩🏼‍🦲", "🧑🏼‍🦲", "👨🏼‍🦲", "🧔🏼‍♀️", "🧔🏼", "🧔🏼‍♂️", "👵🏼", "🧓🏼", "👴🏼",
        "👲🏼", "👳🏼‍♀️", "👳🏼", "👳🏼‍♂️", "🧕🏼", "👮🏼‍♀️", "👮🏼", "👮🏼‍♂️", "👷🏼‍♀️",
        "👷🏼", "👷🏼‍♂️", "💂🏼‍♀️", "💂🏼", "💂🏼‍♂️", "🕵🏼‍♀️", "🕵🏼", "🕵🏼‍♂️",
        "👩🏼‍⚕️", "🧑🏼‍⚕️", "👨🏼‍⚕️", "👩🏼‍🌾", "🧑🏼‍🌾", "👨🏼‍🌾", "👩🏼‍🍳", "🧑🏼‍🍳", "👨🏼‍🍳",
        "👩🏼‍🎓", "🧑🏼‍🎓", "👨🏼‍🎓", "👩🏼‍🎤", "🧑🏼‍🎤", "👨🏼‍🎤", "👩🏼‍🏫", "🧑🏼‍🏫", "👨🏼‍🏫",
        "👩🏼‍🏭", "🧑🏼‍🏭", "👨🏼‍🏭", "👩🏼‍💻", "🧑🏼‍💻", "👨🏼‍💻", "👩🏼‍💼", "🧑🏼‍💼", "👨🏼‍💼",
        "👩🏼‍🔧", "🧑🏼‍🔧", "👨🏼‍🔧", "👩🏼‍🔬", "🧑🏼‍🔬", "👨🏼‍🔬", "👩🏼‍🎨", "🧑🏼‍🎨", "👨🏼‍🎨",
        "👩🏼‍🚒", "🧑🏼‍🚒", "👨🏼‍🚒", "👩🏼‍✈️", "🧑🏼‍✈️", "👨🏼‍✈️", "👩🏼‍🚀", "🧑🏼‍🚀", "👨🏼‍🚀",
        "👩🏼‍⚖️", "🧑🏼‍⚖️", "👨🏼‍⚖️", "👰🏼‍♀️", "👰🏼", "👰🏼‍♂️", "🤵🏼‍♀️", "🤵🏼", "🤵🏼‍♂️",
        "👸🏼", "🫅🏼", "🤴🏼", "🥷🏼", "🦸🏼‍♀️", "🦸🏼", "🦸🏼‍♂️", "🦹🏼‍♀️", "🦹🏼", "🦹🏼‍♂️",
        "🤶🏼", "🧑🏼‍🎄", "🎅🏼", "🧙🏼‍♀️", "🧙🏼", "🧙🏼‍♂️", "🧝🏼‍♀️", "🧝🏼", "🧝🏼‍♂️",
        "🧛🏼‍♀️", "🧛🏼", "🧛🏼‍♂️", "🧜🏼‍♀️", "🧜🏼", "🧜🏼‍♂️", "🧚🏼‍♀️", "🧚🏼", "🧚🏼‍♂️",
        "👼🏼", "🤰🏼", "🫄🏼", "🫃🏼", "🤱🏼", "👩🏼‍🍼", "🧑🏼‍🍼", "👨🏼‍🍼", "🙇🏼‍♀️", "🙇🏼",
        "🙇🏼‍♂️", "💁🏼‍♀️", "💁🏼", "💁🏼‍♂️", "🙅🏼‍♀️", "🙅🏼", "🙅🏼‍♂️", "🙆🏼‍♀️", "🙆🏼",
        "🙆🏼‍♂️", "🙋🏼‍♀️", "🙋🏼", "🙋🏼‍♂️", "🧏🏼‍♀️", "🧏🏼", "🧏🏼‍♂️", "🤦🏼‍♀️", "🤦🏼",
        "🤦🏼‍♂️", "🤷🏼‍♀️", "🤷🏼", "🤷🏼‍♂️", "🙎🏼‍♀️", "🙎🏼", "🙎🏼‍♂️", "🙍🏼‍♀️", "🙍🏼",
        "🙍🏼‍♂️", "💇🏼‍♀️", "💇🏼", "💇🏼‍♂️", "💆🏼‍♀️", "💆🏼", "💆🏼‍♂️", "🧖🏼‍♀️", "🧖🏼",
        "🧖🏼‍♂️", "💃🏼", "🕺🏼", "🕴🏼", "👩🏼‍🦽", "🧑🏼‍🦽", "👨🏼‍🦽", "👩🏼‍🦼", "🧑🏼‍🦼", "👨🏼‍🦼",
        "🚶🏼‍♀️", "🚶🏼", "🚶🏼‍♂️", "👩🏼‍🦯", "🧑🏼‍🦯", "👨🏼‍🦯", "🧎🏼‍♀️", "🧎🏼", "🧎🏼‍♂️",
        "🏃🏼‍♀️", "🏃🏼", "🏃🏼‍♂️", "🧍🏼‍♀️", "🧍🏼", "🧍🏼‍♂️", "👭🏼", "🧑🏼‍🤝‍🧑🏼", "👬🏼", "👫🏼",
        "🧗🏼‍♀️", "🧗🏼", "🧗🏼‍♂️", "🏇🏼", "🏂🏼", "🏌🏼‍♀️", "🏌🏼", "🏌🏼‍♂️", "🏄🏼‍♀️", "🏄🏼",
        "🏄🏼‍♂️", "🚣🏼‍♀️", "🚣🏼", "🚣🏼‍♂️", "🏊🏼‍♀️", "🏊🏼", "🏊🏼‍♂️", "⛹🏼‍♀️", "⛹🏼", "⛹🏼‍♂️",
        "🏋🏼‍♀️", "🏋🏼", "🏋🏼‍♂️", "🚴🏼‍♀️", "🚴🏼", "🚴🏼‍♂️", "🚵🏼‍♀️", "🚵🏼", "🚵🏼‍♂️", "🤸🏼‍♀️",
        "🤸🏼", "🤸🏼‍♂️", "🤽🏼‍♀️", "🤽🏼", "🤽🏼‍♂️", "🤾🏼‍♀️", "🤾🏼", "🤾🏼‍♂️", "🤹🏼‍♀️", "🤹🏼",
        "🤹🏼‍♂️", "🧘🏼‍♀️", "🧘🏼", "🧘🏼‍♂️", "🛀🏼", "🛌🏼"
    ],
                                    "food_and_drink_emojis": [
        "🍏", "🍎", "🍐", "🍊", "🍋", "🍌", "🍉", "🍇", "🍓", "🫐",
        "🍈", "🍒", "🍑", "🥭", "🍍", "🥥", "🥝", "🍅", "🍆", "🥑",
        "🥦", "🫛", "🥬", "🥒", "🌶", "🫑", "🌽", "🥕", "🫒", "🧄",
        "🧅", "🫚", "🥔", "🍠", "🥐", "🥯", "🍞", "🥖", "🥨", "🧀",
        "🥚", "🍳", "🧈", "🥞", "🧇", "🥓", "🥩", "🍗", "🍖", "🦴",
        "🌭", "🍔", "🍟", "🍕", "🫓", "🥪", "🥙", "🧆", "🌮", "🌯",
        "🫔", "🥗", "🥘", "🫕", "🥫", "🍝", "🍜", "🍲", "🍛", "🍣",
        "🍱", "🥟", "🦪", "🍤", "🍙", "🍚", "🍘", "🍥", "🥠", "🥮",
        "🍢", "🍡", "🍧", "🍨", "🍦", "🥧", "🧁", "🍰", "🎂", "🍮",
        "🍭", "🍬", "🍫", "🍿", "🍩", "🍪", "🌰", "🥜", "🍯", "🥛",
        "🍼", "🫖", "☕️", "🍵", "🧃", "🥤", "🧋", "🫙", "🍶", "🍺",
        "🍻", "🥂", "🍷", "🫗", "🥃", "🍸", "🍹", "🧉", "🍾", "🧊",
        "🥄", "🍴", "🍽", "🥣", "🥡", "🥢", "🧂"
    ],     "Activity and Sports": [
        "⚽️", "🏀", "🏈", "⚾️", "🥎", "🎾", "🏐", "🏉", "🥏", "🎱", "🪀",
        "🏓", "🏸", "🏒", "🏑", "🥍", "🏏", "🪃", "🥅", "⛳️", "🪁", "🏹", "🎣",
        "🤿", "🥊", "🥋", "🎽", "🛹", "🛼", "🛷", "⛸", "🥌", "🎿", "⛷", "🏂",
        "🪂", "🏋️‍♀️", "🏋️", "🏋️‍♂️", "🤼‍♀️", "🤼", "🤼‍♂️", "🤸‍♀️", "🤸", "🤸‍♂️",
        "⛹️‍♀️", "⛹️", "⛹️‍♂️", "🤺", "🤾‍♀️", "🤾", "🤾‍♂️", "🏌️‍♀️", "🏌️", "🏌️‍♂️",
        "🏇", "🧘‍♀️", "🧘", "🧘‍♂️", "🏄‍♀️", "🏄", "🏄‍♂️", "🏊‍♀️", "🏊", "🏊‍♂️",
        "🤽‍♀️", "🤽", "🤽‍♂️", "🚣‍♀️", "🚣", "🚣‍♂️", "🧗‍♀️", "🧗", "🧗‍♂️",
        "🚵‍♀️", "🚵", "🚵‍♂️", "🚴‍♀️", "🚴", "🚴‍♂️", "🏆", "🥇", "🥈",
        "🥉", "🏅", "🎖", "🏵", "🎗", "🎫", "🎟", "🎪", "🤹", "🤹‍♂️",
        "🤹‍♀️", "🎭", "🩰", "🎨", "🎬", "🎤", "🎧", "🎼", "🎹", "🥁",
        "🪘", "🪇", "🎷", "🎺", "🪗", "🎸", "🪕", "🎻", "🪈", "🎲",
        "♟", "🎯", "🎳", "🎮", "🎰", "🧩"
    ],     "Travel & Places": [
        "🚗", "🚕", "🚙", "🚌", "🚎", "🏎", "🚓", "🚑", "🚒", "🚐", "🛻", "🚚",
        "🚛", "🚜", "🦯", "🦽", "🦼", "🛴", "🚲", "🛵", "🏍", "🛺", "🚨", "🚔",
        "🚍", "🚘", "🚖", "🛞", "🚡", "🚠", "🚟", "🚃", "🚋", "🚞", "🚝", "🚄",
        "🚅", "🚈", "🚂", "🚆", "🚇", "🚊", "🚉", "✈️", "🛫", "🛬", "🛩", "💺",
        "🛰", "🚀", "🛸", "🚁", "🛶", "⛵️", "🚤", "🛥", "🛳", "⛴", "🚢", "⚓️",
        "🛟", "🪝", "⛽️", "🚧", "🚦", "🚥", "🚏", "🗺", "🗿", "🗽", "🗼", "🏰",
        "🏯", "🏟", "🎡", "🎢", "🛝", "🎠", "⛲️", "⛱", "🏖", "🏝", "🏜", "🌋",
        "⛰", "🏔", "🗻", "🏕", "⛺️", "🛖", "🏠", "🏡", "🏘", "🏚", "🏗", "🏭",
        "🏢", "🏬", "🏣", "🏤", "🏥", "🏦", "🏨", "🏪", "🏫", "🏩", "💒", "🏛",
        "⛪️", "🕌", "🕍", "🛕", "🕋", "⛩", "🛤", "🛣", "🗾", "🎑", "🏞", "🌅",
        "🌄", "🌠", "🎇", "🎆", "🌇", "🌆", "🏙", "🌃", "🌌", "🌉", "🌁"
    ] ,     "Objects": [
        "⌚️", "📱", "📲", "💻", "⌨️", "🖥", "🖨", "🖱", "🖲", "🕹", "🗜", "💽",
        "💾", "💿", "📀", "📼", "📷", "📸", "📹", "🎥", "📽", "🎞", "📞", "☎️",
        "📟", "📠", "📺", "📻", "🎙", "🎚", "🎛", "🧭", "⏱", "⏲", "⏰", "🕰",
        "⌛️", "⏳", "📡", "🔋", "🪫", "🔌", "💡", "🔦", "🕯", "🪔", "🧯", "🛢",
        "🛍️", "💸", "💵", "💴", "💶", "💷", "🪙", "💰", "💳", "💎", "⚖️", "🪮",
        "🪜", "🧰", "🪛", "🔧", "🔨", "⚒", "🛠", "⛏", "🪚", "🔩", "⚙️", "🪤",
        "🧱", "⛓", "🧲", "🔫", "💣", "🧨", "🪓", "🔪", "🗡", "⚔️", "🛡", "🚬",
        "⚰️", "🪦", "⚱️", "🏺", "🔮", "📿", "🧿", "🪬", "💈", "⚗️", "🔭", "🔬",
        "🕳", "🩹", "🩺", "🩻", "🩼", "💊", "💉", "🩸", "🧬", "🦠", "🧫", "🧪",
        "🌡", "🧹", "🪠", "🧺", "🧻", "🚽", "🚰", "🚿", "🛁", "🛀", "🧼", "🪥",
        "🪒", "🧽", "🪣", "🧴", "🛎", "🔑", "🗝", "🚪", "🪑", "🛋", "🛏", "🛌",
        "🧸", "🪆", "🖼", "🪞", "🪟", "🛍", "🛒", "🎁", "🎈", "🎏", "🎀", "🪄",
        "🪅", "🎊", "🎉", "🪩", "🎎", "🏮", "🎐", "🧧", "✉️", "📩", "📨", "📧",
        "💌", "📥", "📤", "📦", "🏷", "🪧", "📪", "📫", "📬", "📭", "📮", "📯",
        "📜", "📃", "📄", "📑", "🧾", "📊", "📈", "📉", "🗒", "🗓", "📆", "📅",
        "🗑", "🪪", "📇", "🗃", "🗳", "🗄", "📋", "📁", "📂", "🗂", "🗞", "📰",
        "📓", "📔", "📒", "📕", "📗", "📘", "📙", "📚", "📖", "🔖", "🧷", "🔗",
        "📎", "🖇", "📐", "📏", "🧮", "📌", "📍", "✂️", "🖊", "🖋", "✒️", "🖌",
        "🖍", "📝", "✏️", "🔍", "🔎", "🔏", "🔐", "🔒", "🔓"
    ] ,     "Symbols": [
        "❤️", "🩷", "🧡", "💛", "💚", "💙", "🩵", "💜", "🖤", "🩶", "🤍", "🤎",
        "❤️‍🔥", "❤️‍🩹", "💔", "❣️", "💕", "💞", "💓", "💗", "💖", "💘", "💝",
        "💟", "☮️", "✝️", "☪️", "🪯", "🕉", "☸️", "✡️", "🔯", "🕎", "☯️",
        "☦️", "🛐", "⛎", "♈️", "♉️", "♊️", "♋️", "♌️", "♍️", "♎️", "♏️",
        "♐️", "♑️", "♒️", "♓️", "🆔", "⚛️", "🉑", "☢️", "☣️", "📴", "📳",
        "🈶", "🈚️", "🈸", "🈺", "🈷️", "✴️", "🆚", "💮", "🉐", "㊙️", "㊗️",
        "🈴", "🈵", "🈹", "🈲", "🅰️", "🅱️", "🆎", "🆑", "🅾️", "🆘", "❌",
        "⭕️", "🛑", "⛔️", "📛", "🚫", "💯", "💢", "♨️", "🚷", "🚯", "🚳",
        "🚱", "🔞", "📵", "🚭", "❗️", "❕", "❓", "❔", "‼️", "⁉️", "🔅",
        "🔆", "〽️", "⚠️", "🚸", "🔱", "⚜️", "🔰", "♻️", "✅", "🈯️", "💹",
        "❇️", "✳️", "❎", "🌐", "💠", "Ⓜ️", "🌀", "💤", "🏧", "🚾", "♿️",
        "🅿️", "🛗", "🈳", "🈂️", "🛂", "🛃", "🛄", "🛅", "🚹", "🚺", "🚼",
        "⚧", "🚻", "🚮", "🎦", "🛜", "📶", "🈁", "🔣", "ℹ️", "🔤", "🔡",
        "🔠", "🆖", "🆗", "🆙", "🆒", "🆕", "🆓", "0️⃣", "1️⃣", "2️⃣", "3️⃣",
        "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟", "🔢", "#️⃣", "*️⃣",
        "⏏️", "▶️", "⏸", "⏯", "⏹", "⏺", "⏭", "⏮", "⏩", "⏪", "⏫",
        "⏬", "◀️", "🔼", "🔽", "➡️", "⬅️", "⬆️", "⬇️", "↗️", "↘️", "↙️",
        "↖️", "↕️", "↔️", "↪️", "↩️", "⤴️", "⤵️", "🔀", "🔁", "🔂", "🔄",
        "🔃", "🎵", "🎶", "➕", "➖", "➗", "✖️", "🟰", "♾", "💲", "💱",
        "™️", "©️", "®️", "〰️", "➰", "➿", "🔚", "🔙", "🔛", "🔝", "🔜",
        "✔️", "☑️", "🔘", "🔴", "🟠", "🟡", "🟢", "🔵", "🟣", "⚫️", "⚪️",
        "🟤", "🔺", "🔻", "🔸", "🔹", "🔶", "🔷", "🔳", "🔲", "▪️", "▫️",
        "◾️", "◽️", "◼️", "◻️", "🟥", "🟧", "🟨", "🟩", "🟦", "🟪", "⬛️",
        "⬜️", "🟫", "🔈", "🔇", "🔉", "🔊", "🔔", "🔕", "📣", "📢", "👁‍🗨",
        "💬", "💭", "🗯", "♠️", "♣️", "♥️", "♦️", "🃏", "🎴", "🀄️", "🕐",
        "🕑", "🕒", "🕓", "🕔", "🕕", "🕖", "🕗", "🕘", "🕙", "🕚", "🕛",
        "🕜", "🕝", "🕞", "🕟", "🕠", "🕡", "🕢", "🕣", "🕤", "🕥", "🕦",
        "🕧"
    ] ,     "Flags": [
        "🏳️", "🏴", "🏁", "🚩", "🏳️‍🌈", "🏳️‍⚧️", "🏴‍☠️", "🇦🇫", "🇦🇽", "🇦🇱",
        "🇩🇿", "🇦🇸", "🇦🇩", "🇦🇴", "🇦🇮", "🇦🇶", "🇦🇬", "🇦🇷", "🇦🇲", "🇦🇼",
        "🇦🇺", "🇦🇹", "🇦🇿", "🇧🇸", "🇧🇭", "🇧🇩", "🇧🇧", "🇧🇾", "🇧🇪", "🇧🇿",
        "🇧🇯", "🇧🇲", "🇧🇹", "🇧🇴", "🇧🇦", "🇧🇼", "🇧🇷", "🇮🇴", "🇻🇬", "🇧🇳",
        "🇧🇬", "🇧🇫", "🇧🇮", "🇰🇭", "🇨🇲", "🇨🇦", "🇮🇨", "🇨🇻", "🇧🇶", "🇰🇾",
        "🇨🇫", "🇹🇩", "🇨🇱", "🇨🇳", "🇨🇽", "🇨🇨", "🇨🇴", "🇰🇲", "🇨🇬", "🇨🇩",
        "🇨🇰", "🇨🇷", "🇨🇮", "🇭🇷", "🇨🇺", "🇨🇼", "🇨🇾", "🇨🇿", "🇩🇰", "🇩🇯",
        "🇩🇲", "🇩🇴", "🇪🇨", "🇪🇬", "🇸🇻", "🇬🇶", "🇪🇷", "🇪🇪", "🇪🇹", "🇪🇺",
        "🇫🇰", "🇫🇴", "🇫🇯", "🇫🇮", "🇫🇷", "🇬🇫", "🇵🇫", "🇹🇫", "🇬🇦", "🇬🇲",
        "🇬🇪", "🇩🇪", "🇬🇭", "🇬🇮", "🇬🇷", "🇬🇱", "🇬🇩", "🇬🇵", "🇬🇺", "🇬🇹",
        "🇬🇬", "🇬🇳", "🇬🇼", "🇬🇾", "🇭🇹", "🇭🇳", "🇭🇰", "🇭🇺", "🇮🇸", "🇮🇳",
        "🇮🇩", "🇮🇷", "🇮🇶", "🇮🇪", "🇮🇲", "🇮🇱", "🇮🇹", "🇯🇲", "🇯🇵", "🎌",
        "🇯🇪", "🇯🇴", "🇰🇿", "🇰🇪", "🇰🇮", "🇽🇰", "🇰🇼", "🇰🇬", "🇱🇦", "🇱🇻",
        "🇱🇧", "🇱🇸", "🇱🇷", "🇱🇾", "🇱🇮", "🇱🇹", "🇱🇺", "🇲🇴", "🇲🇰", "🇲🇬",
        "🇲🇼", "🇲🇾", "🇲🇻", "🇲🇱", "🇲🇹", "🇲🇭", "🇲🇶", "🇲🇷", "🇲🇺", "🇾🇹",
        "🇲🇽", "🇫🇲", "🇲🇩", "🇲🇨", "🇲🇳", "🇲🇪", "🇲🇸", "🇲🇦", "🇲🇿", "🇲🇲",
        "🇳🇦", "🇳🇷", "🇳🇵", "🇳🇱", "🇳🇨", "🇳🇿", "🇳🇮", "🇳🇪", "🇳🇬", "🇳🇺",
        "🇳🇫", "🇰🇵", "🇲🇵", "🇳🇴", "🇴🇲", "🇵🇰", "🇵🇼", "🇵🇸", "🇵🇦", "🇵🇬",
        "🇵🇾", "🇵🇪", "🇵🇭", "🇵🇳", "🇵🇱", "🇵🇹", "🇵🇷", "🇶🇦", "🇷🇪", "🇷🇴",
        "🇷🇺", "🇷🇼", "🇼🇸", "🇸🇲", "🇸🇦", "🇸🇳", "🇷🇸", "🇸🇨", "🇸🇱", "🇸🇬",
        "🇸🇽", "🇸🇰", "🇸🇮", "🇬🇸", "🇸🇧", "🇸🇴", "🇿🇦", "🇰🇷", "🇸🇸", "🇪🇸",
        "🇱🇰", "🇧🇱", "🇸🇭", "🇰🇳", "🇱🇨", "🇵🇲", "🇻🇨", "🇸🇩", "🇸🇷", "🇸🇿",
        "🇸🇪", "🇨🇭", "🇸🇾", "🇹🇼", "🇹🇯", "🇹🇿", "🇹🇭", "🇹🇱", "🇹🇬", "🇹🇰",
        "🇹🇴", "🇹🇹", "🇹🇳", "🇹🇷", "🇹🇲", "🇹🇨", "🇹🇻", "🇻🇮", "🇺🇬", "🇺🇦",
        "🇦🇪", "🇬🇧", "🏴󠁧󠁢󠁥󠁮󠁧󠁿", "🏴󠁧󠁢󠁳󠁣󠁴󠁿", "🏴󠁧󠁢󠁷󠁬󠁳󠁿", "🇺🇳", "🇺🇸",
        "🇺🇾", "🇺🇿", "🇻🇺", "🇻🇦", "🇻🇪", "🇻🇳", "🇼🇫", "🇪🇭", "🇾🇪", "🇿🇲",
        "🇿🇼"
    ]
    ,
    "new_emojis" : [
    "🫨", "🩷", "🩵", "🩶", "🫸", "🫸🏻", "🫸🏼", "🫸🏽", "🫸🏾", "🫸🏿", "🫷", "🫷🏻",
    "🫷🏼", "🫷🏽", "🫷🏾", "🫷🏿", "🫏", "🫎", "🪿", "🐦‍⬛", "🪽", "🪼", "🪻", "🫛",
    "🫚", "🪭", "🪮", "🪈", "🪇", "🫠", "🫢", "🫣", "🫡", "🫥", "🫤", "🥹", "🫱", "🫱🏻",
    "🫱🏼", "🫱🏽", "🫱🏾", "🫱🏿", "🫲", "🫲🏻", "🫲🏼", "🫲🏽", "🫲🏾", "🫲🏿", "🫳",
    "🫳🏻", "🫳🏼", "🫳🏽", "🫳🏾", "🫳🏿", "🫴", "🫴🏻", "🫴🏼", "🫴🏽", "🫴🏾", "🫴🏿",
    "🫰", "🫰🏻", "🫰🏼", "🫰🏽", "🫰🏾", "🫰🏿", "🫵", "🫵🏻", "🫵🏼", "🫵🏽", "🫵🏾",
    "🫵🏿", "🫶", "🫶🏻", "🫶🏼", "🫶🏽", "🫶🏾", "🫶🏿", "🤝🏻", "🤝🏼", "🤝🏽", "🤝🏾",
    "🤝🏿", "🫱🏻‍🫲🏼", "🫱🏻‍🫲🏽", "🫱🏻‍🫲🏾", "🫱🏻‍🫲🏿", "🫱🏼‍🫲🏻", "🫱🏼‍🫲🏽",
    "🫱🏼‍🫲🏾", "🫱🏼‍🫲🏿", "🫱🏽‍🫲🏻", "🫱🏽‍🫲🏼", "🫱🏽‍🫲🏾", "🫱🏽‍🫲🏿", "🫱🏾‍🫲🏻",
    "🫱🏾‍🫲🏼", "🫱🏾‍🫲🏽", "🫱🏾‍🫲🏿", "🫱🏿‍🫲🏻", "🫱🏿‍🫲🏼", "🫱🏿‍🫲🏽", "🫱🏿‍🫲🏾",
    "🫦", "🫅", "🫅🏻", "🫅🏼", "🫅🏽", "🫅🏾", "🫅🏿", "🫃", "🫃🏻", "🫃🏼", "🫃🏽",
    "🫃🏾", "🫃🏿", "🫄", "🫄🏻", "🫄🏼", "🫄🏽", "🫄🏾", "🫄🏿", "🧌", "🪸", "🪷", "🪹",
    "🪺", "🫘", "🫗", "🫙", "🛝", "🛞", "🛟", "🪬", "🪩", "🪫", "🩼", "🩻", "🫧", "🪪",
    "🟰😮‍💨", "😵‍💫", "😶‍🌫️", "❤️‍🔥", "❤️‍🩹", "🧔‍♀️", "🧔🏻‍♀️", "🧔🏼‍♀️",
    "🧔🏽‍♀️", "🧔🏾‍♀️", "🧔🏿‍♀️", "🧔‍♂️", "🧔🏻‍♂️", "🧔🏼‍♂️", "🧔🏽‍♂️",
    "🧔🏾‍♂️", "🧔🏿‍♂️"
]
        }
        self.current_category = "Smileys"
        layout = QtWidgets.QGridLayout()
        self.setLayout(layout)
        self.init_ui()

    def init_ui(self):
        while self.layout().count() > 0:
            item = self.layout().takeAt(0)
            widget = item.widget()
            if widget:
                widget.deleteLater()

        scroll_area = QtWidgets.QScrollArea()
        scroll_content = QtWidgets.QWidget()
        scroll_layout = QtWidgets.QGridLayout(scroll_content)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(scroll_content)
        self.layout().addWidget(scroll_area)

        emojis = self.categories[self.current_category]
        num_columns = 7  # Number of columns you want
        row, col = 0, 0
        button_size = 30
        emoji_style = f"font-size: {button_size}px;"
        emoji_font_size = button_size - 10  # Adjust the font size of emojis
        for emoji in emojis:
            button = QtWidgets.QPushButton(emoji)
            button.setStyleSheet(emoji_style)
            button.setFont(QtGui.QFont("Noto Color Emoji", emoji_font_size))
            button.clicked.connect(partial(self.insert_emoji, emoji))
            scroll_layout.addWidget(button, row, col)
            col += 1
            if col >= num_columns:
                col = 0
                row += 1

        scroll_layout.setColumnStretch(num_columns, 1)  # Add column stretch to fill remaining space



    def insert_emoji(self, emoji):
        cursor = self.parent().text_edit.textCursor()
        cursor.insertText(emoji)
        self.parent().text_edit.setTextCursor(cursor)

class TextEditorApp(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def close_emoji_picker(self):
        self.emoji_picker.hide()

    def init_ui(self):
        self.setWindowTitle("Text Editor with Emoji Picker")
        self.setGeometry(100, 100, 800, 600)

        self.text_edit = QtWidgets.QTextEdit(self)
        self.text_edit.setFont(QtGui.QFont("Noto Color Emoji", 18))
        self.setCentralWidget(self.text_edit)

        self.emoji_picker = EmojiPicker(parent=self)
        self.emoji_picker.setGeometry(10, 50, 400, 300)
        self.emoji_picker.hide()

        emoji_button = QtWidgets.QPushButton("😊", self)
        emoji_button.setFont(QtGui.QFont("Segoe UI Emoji", 18))
        emoji_button.setToolTip("Insert Emoji")
        emoji_button.setGeometry(10, 10, 30, 30)
        emoji_button.clicked.connect(self.toggle_emoji_picker)

        category_dropdown = QtWidgets.QComboBox(self)
        category_dropdown.setGeometry(50, 10, 150, 30)
        category_dropdown.addItems(list(self.emoji_picker.categories.keys()))
        category_dropdown.currentIndexChanged.connect(self.change_emoji_category)

        close_button = QtWidgets.QPushButton("❌", self.emoji_picker)
        close_button.setGeometry(370, 10, 30, 30)
        close_button.setFont(QtGui.QFont("Arial", 18))
        close_button.clicked.connect(self.close_emoji_picker)

    def toggle_emoji_picker(self):
        if self.emoji_picker.isVisible():
            self.emoji_picker.hide()
        else:
            self.emoji_picker.show()

    def change_emoji_category(self, index):
        category = list(self.emoji_picker.categories.keys())[index]
        self.emoji_picker.current_category = category
        self.emoji_picker.init_ui()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = TextEditorApp()
    window.show()
    sys.exit(app.exec_())
