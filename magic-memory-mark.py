#pip install PySide2 markdown2
import sys
import webbrowser
import io
import markdown2
import http.server
import socketserver
import threading

from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QVBoxLayout, QWidget, QAction, QFileDialog, QFontComboBox, QToolBar, QMessageBox, QSizePolicy


from PySide2.QtGui import QKeySequence, QColor, QPalette, QTextCursor, QTextCharFormat, QFont, QSyntaxHighlighter, QIcon, QKeyEvent

from PySide2.QtCore import Qt, QRegularExpression
from PySide2.QtGui import QTextCharFormat

import tkinter as tk
from tkinter import messagebox

class CoolInMemoryTextEditor(QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()
        self.http_server = None
    def increase_font_size(self):
        current_font = self.text_widget.currentFont()
        point_size = current_font.pointSize()
        current_font.setPointSize(point_size + 6)
        self.text_widget.setCurrentFont(current_font)

    def decrease_font_size(self):
        current_font = self.text_widget.currentFont()
        point_size = current_font.pointSize()
        current_font.setPointSize(point_size - 3)
        self.text_widget.setCurrentFont(current_font)
##############3
    def change_font_family(self, font):
        current_font = self.text_widget.currentFont()
        current_font.setFamily(font.family())
        self.text_widget.setCurrentFont(current_font)
#################
    def init_ui(self):
        self.setWindowTitle("Cool In-Memory Text Editor")
        self.setGeometry(100, 100, 800, 600)

        # Set PeachPuff background color
        self.set_peachpuff_style()

        # Create the text widget
        self.text_widget = QTextEdit()
        self.text_widget.setFontPointSize(18)

        # Create actions for the toolbar and menu
        self.save_action = QAction(QIcon.fromTheme("document-save"), "Save", self)
        self.save_action.triggered.connect(self.save_text)
        self.save_action.setShortcut(QKeySequence.Save)

        self.open_action = QAction(QIcon.fromTheme("document-open"), "Open", self)
        self.open_action.triggered.connect(self.open_file)
        self.open_action.setShortcut(QKeySequence.Open)

        # Create the toolbar
        self.toolbar = QToolBar("Toolbar")
        self.toolbar.setStyleSheet(
    """
    QToolBar {
        background-color: peachpuff;
        border: 2px solid white;
        border-radius: 5px;
        padding: 5px;
    }

    QToolButton {
        background-color: #ff6b6b;
        color: white;
        border: none;
        padding: 5px;
        border-radius: 5px;
    }

    QToolButton:hover {
        border-radius: 10px;
        background-color: #3a1c71;

    }
    """
)
        self.toolbar.addAction(self.save_action)
        self.toolbar.addAction(self.open_action)

        # Change the Font Family
        self.font_family_combobox = QFontComboBox(self)
        self.font_family_combobox.currentFontChanged.connect(self.change_font_family)
        self.toolbar.addWidget(self.font_family_combobox)
        self.text_widget.setFontPointSize(18)

        # Bold Button
        self.bold_action = QAction(QIcon.fromTheme("format-text-bold"), "Bold", self)
        self.bold_action.triggered.connect(self.toggle_bold)
        self.bold_action.setShortcut(QKeySequence.Bold)
        self.toolbar.addAction(self.bold_action)
        self.addToolBar(self.toolbar)

        # Set the central widget and layout
        layout = QVBoxLayout()
        layout.addWidget(self.text_widget)
        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)

        # Markdown Preview Button
        self.preview_action = QAction(QIcon.fromTheme("text-html"), "Markdown Preview", self)
        self.preview_action.triggered.connect(self.preview_markdown)
        self.toolbar.addAction(self.preview_action)
        self.addToolBar(self.toolbar)


        # Markdown syntax highlighting
        self.highlighter = MarkdownHighlighter(self.text_widget.document())


        # Increase Font Size Button
        self.increase_font_action = QAction(QIcon.fromTheme("format-font-size-more"), "+", self)
        self.increase_font_action.triggered.connect(self.increase_font_size)
        self.increase_font_action.setShortcut(QKeySequence("Ctrl++"))  # Set a keyboard shortcut (Ctrl+ shift + +)
        self.toolbar.addAction(self.increase_font_action)

        # Decrease Font Size Button
        self.decrease_font_action = QAction(QIcon.fromTheme("format-font-size-less"), "-", self)
        self.decrease_font_action.triggered.connect(self.decrease_font_size)
        self.decrease_font_action.setShortcut(QKeySequence("Ctrl+-"))  # Set a keyboard shortcut (Ctrl + shift + -)
        self.toolbar.addAction(self.decrease_font_action)

        # Add a stretchable space
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.toolbar.addWidget(spacer)

        # Exit Button
        self.exit_action = QAction(QIcon.fromTheme("application-exit"), "Exit", self)
        self.exit_action.triggered.connect(self.confirm_exit)
        self.exit_action.setShortcut(QKeySequence.Quit)
        self.toolbar.addAction(self.exit_action)


    def set_peachpuff_style(self):

        peachpuff_color = QColor(255, 218, 185)
        palette = self.palette()
        palette.setColor(QPalette.Window, peachpuff_color)
        self.setPalette(palette)

    def toggle_bold(self):
        fmt = self.text_widget.currentCharFormat()
        fmt.setFontWeight(QFont.Bold if fmt.fontWeight() == QFont.Normal else QFont.Normal)
        self.text_widget.mergeCurrentCharFormat(fmt)

    def save_text(self):
        text = self.text_widget.toPlainText()
        print("Text in memory:")
        print(text)

    def open_file(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self, "Open File", "", "Text Files (*.txt);;All Files (*)", options=options)
        if file_name:
            try:
                with open(file_name, "r") as file:
                    self.text_widget.setPlainText(file.read())
            except Exception as e:
                print(f"Error opening file: {str(e)}")
    def preview_markdown(self):
        reply = tk.messagebox.askyesno("Warning", "Your note will be served on a local HTTP server. Are you sure you want to proceed?")
        if reply:
            markdown_text = self.text_widget.toPlainText()

            html_content = markdown2.markdown(markdown_text)

            server_thread = threading.Thread(target=self.serve_html_content, args=(html_content,))
            server_thread.daemon = True
            server_thread.start()
        else:
            pass
    def serve_html_content(self, html_content):
        class InMemoryRequestHandler(http.server.SimpleHTTPRequestHandler):
            def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.send_header('Content-length', len(html_content))
                self.end_headers()
                self.wfile.write(html_content.encode('utf-8'))

        with socketserver.TCPServer(("localhost", 0), InMemoryRequestHandler) as httpd:
            self.http_server = httpd
            port = httpd.server_address[1]
            url = f"http://localhost:{port}/index.html"
            webbrowser.open(url)
            httpd.serve_forever()
    def confirm_exit(self):
        reply = tk.messagebox.askyesno("Exit Confirmation", "Are you sure you want to exit?")
        if reply:
            self.close()



class MarkdownHighlighter(QSyntaxHighlighter):
    def __init__(self, parent):
        super().__init__(parent)
        self.highlighting_rules = []

        # Heading
        heading_format = QTextCharFormat()
        heading_format.setFontWeight(QFont.Bold)
        heading_format.setForeground(Qt.darkMagenta)
        self.highlighting_rules.append((QRegularExpression(r'^#{1,6}\s.*$'), heading_format))

        # Italic
        italic_format = QTextCharFormat()
        italic_format.setFontItalic(True)
        italic_format.setForeground(Qt.darkGreen)
        self.highlighting_rules.append((QRegularExpression(r'_(.*?)_'), italic_format))
        self.highlighting_rules.append((QRegularExpression(r'\*(.*?)\*'), italic_format))

        # Bold
        bold_format = QTextCharFormat()
        bold_format.setFontWeight(QFont.Bold)
        bold_format.setForeground(Qt.darkRed)
        self.highlighting_rules.append((QRegularExpression(r'__(.*?)__'), bold_format))
        self.highlighting_rules.append((QRegularExpression(r'\*\*(.*?)\*\*'), bold_format))

        # Strikethrough
        strikethrough_format = QTextCharFormat()
        strikethrough_format.setFontStrikeOut(True)
        strikethrough_format.setForeground(Qt.darkGray)
        self.highlighting_rules.append((QRegularExpression(r'~~(.*?)~~'), strikethrough_format))

        # Code Block
        code_block_format = QTextCharFormat()
        code_block_format.setFontFamily("Courier New")
        code_block_format.setBackground(Qt.lightGray)
        self.highlighting_rules.append((QRegularExpression(r'```[\s\S]*?```'), code_block_format))

        # Inline Code
        inline_code_format = QTextCharFormat()
        inline_code_format.setFontFamily("Courier New")
        inline_code_format.setForeground(Qt.darkBlue)
        self.highlighting_rules.append((QRegularExpression(r'`.*?`'), inline_code_format))

        # Links
        link_format = QTextCharFormat()
        link_format.setForeground(Qt.blue)
        self.highlighting_rules.append((QRegularExpression(r'\[.*?\]\(.*?\)'), link_format))

        # Lists
        list_format = QTextCharFormat()
        list_format.setForeground(Qt.darkCyan)
        self.highlighting_rules.append((QRegularExpression(r'^\s*[-*+]\s.*$'), list_format))

        # Ordered List
        ordered_list_format = QTextCharFormat()
        ordered_list_format.setForeground(Qt.darkCyan)
        self.highlighting_rules.append((QRegularExpression(r'^\s*\d+\.\s.*$'), ordered_list_format))


        # Blockquotes
        quote_format = QTextCharFormat()
        quote_format.setForeground(Qt.darkGreen)
        self.highlighting_rules.append((QRegularExpression(r'^\s*>.*$'), quote_format))

        # Horizontal Rule
        hr_format = QTextCharFormat()
        hr_format.setForeground(Qt.darkGray)
        self.highlighting_rules.append((QRegularExpression(r'^\s*[-*_]{3,}\s*$'), hr_format))


        # Table
        table_format = QTextCharFormat()
        table_format.setForeground(Qt.darkYellow)
        self.highlighting_rules.append((QRegularExpression(r'^\s*(\|.*\|)+\s*$'), table_format))

        # Footnote
        footnote_format = QTextCharFormat()
        footnote_format.setForeground(Qt.darkRed)
        self.highlighting_rules.append((QRegularExpression(r'\[\^\S+\]'), footnote_format))

        # Definition List
        definition_format = QTextCharFormat()
        definition_format.setForeground(Qt.darkMagenta)
        self.highlighting_rules.append((QRegularExpression(r'^\s*.*:\s.*$'), definition_format))

        # Emoji
        emoji_format = QTextCharFormat()
        emoji_format.setForeground(Qt.darkCyan)
        self.highlighting_rules.append((QRegularExpression(r':[a-zA-Z0-9_+-]+:'), emoji_format))

        # Highlight
        highlight_format = QTextCharFormat()
        highlight_format.setBackground(Qt.yellow)
        self.highlighting_rules.append((QRegularExpression(r'==.*?=='), highlight_format))

        # Task Lists
        task_list_format = QTextCharFormat()
        task_list_format.setForeground(Qt.darkCyan)
        self.highlighting_rules.append((QRegularExpression(r'^\s*\[([ xX])\]\s.*$'), task_list_format))

        # Strikethrough (Tilde Syntax)
        tilde_strikethrough_format = QTextCharFormat()
        tilde_strikethrough_format.setFontStrikeOut(True)
        tilde_strikethrough_format.setForeground(Qt.darkGray)
        self.highlighting_rules.append((QRegularExpression(r'~(.*?)~'), tilde_strikethrough_format))

        # Autolinks
        autolink_format = QTextCharFormat()
        autolink_format.setForeground(Qt.blue)
        self.highlighting_rules.append((QRegularExpression(r'<(?:https?://[^>]+)>'), autolink_format))

        # Mentions or References
        mention_format = QTextCharFormat()
        mention_format.setForeground(Qt.darkGreen)
        self.highlighting_rules.append((QRegularExpression(r'@[\w.-]+'), mention_format))

        # Footnote Links in Text
        footnote_link_format = QTextCharFormat()
        footnote_link_format.setForeground(Qt.darkRed)
        self.highlighting_rules.append((QRegularExpression(r'\[\^\S+\]'), footnote_link_format))

        ###################################
    def highlightBlock(self, text):
        for pattern, format in self.highlighting_rules:
            expression = QRegularExpression(pattern)
            it = expression.globalMatch(text)
            while it.hasNext():
                match = it.next()
                self.setFormat(match.capturedStart(), match.capturedLength(), format)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    editor = CoolInMemoryTextEditor()
    editor.show()
    sys.exit(app.exec_())

