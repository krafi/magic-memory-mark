extends Control
var app_name = "Text Editor"
var current_file = "Untitled"
var Untitled = "Untitled"

func _ready():
	update_window_title()
	for i in ['New File', 'Open File', 'Save as File' , 'Save', 'Quit' ]:
		$MenuButtonFile.get_popup().add_item(i)
	for i in ['Issue Report', 'About' ]:
		$MenuButtonHelp.get_popup().add_item(i)
		
	#$MenuButtonFile.get_popup().connect("id_pressed", self, "_on_item_pressed") #Godot 3
	$MenuButtonFile.get_popup().id_pressed.connect(self._on_item_pressed)
	$MenuButtonHelp.get_popup().id_pressed.connect(self._on_item_help_pressed)
	
	#$MenuButtonFile.get_popup().set_item_shortcut(4,set_shortcut(KEY_Q), true)

func set_shortcut(key):
	var shortcut = Shortcut.new()
	var inputeventkey = InputEventKey.new()
	inputeventkey.set_scancode(key) # issue
	inputeventkey.control = true
	shortcut.set_shortcut(inputeventkey)
	return shortcut
func new_file():
	current_file = Untitled
	update_window_title()
	$TextEdit.text = "Do not use single quotation mark ('), remove this line"
	
func update_window_title():
	#OS.set_window_title(app_name + " - " + current_file)
	DisplayServer.window_set_title(app_name + " - " + current_file)

func _on_item_pressed(id):
	var item_name = $MenuButtonFile.get_popup().get_item_text(id)
	print(item_name + ' pressed ')
	match item_name:
		'Open File':
			$FileDialog.popup()
		'Save':
			save_file()
		'New File':
			new_file()
		'Save as File':
			$SaveFileDialog.popup()
		'Quit':
			get_tree().quit()

func _on_item_help_pressed(id):
	var item_name = $MenuButtonHelp.get_popup().get_item_text(id)

	match item_name:
		'About':
			$MenuButtonHelp/AboutDialog.popup()
		'Issue Report':
			OS.shell_open("https://krafi.info")

func _on_open_file_pressed():
	$FileDialog.popup()
func _on_save_file_pressed():
	$SaveFileDialog.popup()
	
func _on_file_dialog_file_selected(path): # open
	print(path)
	var f = FileAccess.open(path,FileAccess.READ)
	f.open(path, 1)
	#$TextEdit.text = f.get_as_text()

	var stdout = []
	var password = "password"
	var VarDataDecryptor = OS.execute('python3', ['VarDataEncryptor.py', 'dec', f.get_as_text() , password], stdout)
	if VarDataDecryptor == OK:
		var output = str(stdout)
		var processed_output = output.substr(2, output.length() - 4) # Removing square brackets and quotes
		processed_output = processed_output.replace("\\n", "\n") # Replace "\n" with actual line breaks
		#print(processed_output)
		$TextEdit.text = processed_output

	f.close()
	current_file = path
	print("====+++======")
	update_window_title()

func _on_save_file_dialog_file_selected(path): # save as
	var f = FileAccess.open(path,FileAccess.WRITE)
	f.open(path , 2)
	var processed_output =_VarDataEncryptor()
	f.store_string(processed_output)
	f.close()
	current_file = path
	update_window_title()
func save_file(): # save
	var processed_output =_VarDataEncryptor()
	print(processed_output)
	
	var path = current_file
	if path == Untitled:
		$SaveFileDialog.popup() # forward to save as
	else:
		var f = FileAccess.open(path,FileAccess.WRITE) # save
		
		f.open(path , 2)
		#f.store_string($TextEdit.text)
		f.store_string(processed_output)
		f.close()
		current_file = path
		update_window_title()

func _VarDataEncryptor():
	var text_to_send = $TextEdit.text
	#var new_text_to_send = text_to_send.replace("'","*")
	
	var stdout = []
	var password = "password"
	var VarDataDecryptor = OS.execute('python3', ['VarDataEncryptor.py', 'enc', text_to_send , password], stdout)
	if VarDataDecryptor == OK:
		var output = str(stdout)
		var processed_output = output.substr(2, output.length() - 4) # Removing square brackets and quotes
		processed_output = processed_output.replace("\\n", "\n") # Replace "\n" with actual line breaks
		return processed_output

func _on_button_pressed():
	var processed_output =_VarDataEncryptor()
	print(processed_output)

