import sys
import hashlib
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Protocol.KDF import PBKDF2

def derive_key(password, salt, key_length):
    return PBKDF2(password, salt, dkLen=key_length, count=100000)

def pad(data, block_size):
    padding = block_size - len(data) % block_size
    return data + bytes([padding] * padding)

def unpad(data):
    padding = data[-1]
    return data[:-padding]

def encrypt_data(data, password):
    salt = get_random_bytes(16)
    key = derive_key(password, salt, 32)
    iv = get_random_bytes(16)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    plaintext = data.encode()
    padded_plaintext = pad(plaintext, 16)
    encrypted_data = cipher.encrypt(padded_plaintext)
    enc_binary_data = b'ENCRYPTED:' + salt + iv + encrypted_data
    encrypted_hex = enc_binary_data.hex()
    return encrypted_hex


def decrypt_data(encrypted_data, password):
    encrypted_binary_data = bytes.fromhex(encrypted_data)
    if not encrypted_binary_data.startswith(b'ENCRYPTED:'):
        raise ValueError("The data is not encrypted with this program.")
    encrypted_binary_data = encrypted_binary_data[len(b'ENCRYPTED:'):]
    salt = encrypted_binary_data[:16]
    iv = encrypted_binary_data[16:32]
    ciphertext = encrypted_binary_data[32:]
    key = derive_key(password, salt, 32)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    decrypted_data = unpad(cipher.decrypt(ciphertext))
    return decrypted_data.decode()

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Usage: python3 test.py <"enc" or "dec"> <"data"> <"passwd"> ')
    else:

        worktype = sys.argv[1]
        data = sys.argv[2]
        passwd = sys.argv[3]
        if worktype == "enc":
            #echo -n "your_data" | openssl enc -aes-256-cbc -salt -pbkdf2 -pass pass:"your_password" -out encrypted.bin

            enc = encrypt_data(data, passwd)
            print(enc)
        else:
            #openssl enc -d -aes-256-cbc -in encrypted.bin -salt -pbkdf2 -pass pass:"your_password"

            dec = decrypt_data(data, passwd)
            print(dec)
