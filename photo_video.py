import tempfile
from datetime import datetime
import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QVBoxLayout, QWidget, QSlider, QPushButton
from PySide2.QtCore import Qt
from PySide2.QtGui import QPixmap
from PySide2.QtMultimedia import QMediaPlayer, QMediaContent
from PySide2.QtMultimediaWidgets import QVideoWidget
from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QVBoxLayout, QWidget, QSlider, QPushButton, QInputDialog
from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QVBoxLayout, QWidget, QSlider, QPushButton, QInputDialog, QLineEdit
from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QVBoxLayout, QWidget, QSlider, QPushButton, QInputDialog, QMessageBox, QLineEdit
from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QVBoxLayout, QWidget, QSlider, QPushButton, QSpacerItem, QInputDialog, QLineEdit, QMessageBox
from PySide2.QtWidgets import QApplication, QMainWindow, QTextEdit, QLabel, QVBoxLayout, QWidget, QSlider, QPushButton, QInputDialog, QLineEdit, QFileDialog, QMessageBox

from PySide2.QtCore import QUrl
import vlc
import os
import os
import hashlib
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# Create directories for videos and images
video_dir = './dir/videos'
image_dir = './dir/images'
os.makedirs(video_dir, exist_ok=True)
os.makedirs(image_dir, exist_ok=True)

class TextEditor(QMainWindow):

    def unpad(self, data):
        padding = data[-1]
        return data[:-padding]

    def pad(self, data, block_size):
        padding = block_size - len(data) % block_size
        return data + bytes([padding] * padding)
    def encrypt_var_data(self, data, password):
        block_size = 16
        key = hashlib.sha256(password.encode()).digest()
        iv = get_random_bytes(block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)


    # Pad the data
        padded_data = self.pad(data, block_size)
        encrypted_data = cipher.encrypt(padded_data)
        return b'ENCRYPTED:' + iv + encrypted_data

    def decrypt_var_data(self, encrypted_data, password):
        block_size = 16
        key = hashlib.sha256(password.encode()).digest()

        if not encrypted_data.startswith(b'ENCRYPTED:'):
            raise ValueError("The data is not encrypted with this program.")

        encrypted_data = encrypted_data[len(b'ENCRYPTED:'):]
        iv = encrypted_data[:block_size]
        ciphertext = encrypted_data[block_size:]
        cipher = AES.new(key, AES.MODE_CBC, iv)

        decrypted_data = self.unpad(cipher.decrypt(ciphertext))
        return decrypted_data

    def choose_restore_directory(self):
        self.restore_dir = QFileDialog.getExistingDirectory(self, "Choose Restore Directory")
        if self.restore_dir:
            self.decrypt_photo_button.setEnabled(True)
            self.decrypt_video_button.setEnabled(True)




    def encrypt_and_save(self, file_path, password, is_video, target_dir):
        with open(file_path, 'rb') as file:
            data = file.read()
            encrypted_data = self.encrypt_var_data(data, password)
            encrypted_filename = os.path.basename(file_path)
            encrypted_path = os.path.join(target_dir, encrypted_filename)
            with open(encrypted_path, 'wb') as encrypted_file:
                encrypted_file.write(encrypted_data)





    def restore_video(self, decrypted_data, encrypted_filename, restore_dir):
        restored_path = os.path.join(restore_dir, encrypted_filename)
        with open(restored_path, 'wb') as restored_file:
            restored_file.write(decrypted_data)
        QMessageBox.information(self, "Restoration", f"Video '{encrypted_filename}' restored to '{restore_dir}'.")

    def decrypt_and_display(self, encrypted_path, password, restore_dir):
        with open(encrypted_path, 'rb') as encrypted_file:
            encrypted_data = encrypted_file.read()
            decrypted_data = self.decrypt_var_data(encrypted_data, password)

        if encrypted_path.lower().endswith(('.jpg', '.jpeg', '.png')):
            # Display decrypted image
            pixmap = QPixmap()
            pixmap.loadFromData(decrypted_data)
            self.drop_label.setPixmap(pixmap)
            self.video_widget.hide()
            self.drop_label.show()
        elif encrypted_path.lower().endswith(('.mp4', '.avi')):
            # Create a temporary file for decrypted video data
            with tempfile.NamedTemporaryFile(delete=False) as temp_file:
                temp_file.write(decrypted_data)

            # Create a new media instance using the temporary file's path as the MRL
            media = self.vlc_instance.media_new(temp_file.name)
            self.video_media_instances[encrypted_path] = media
            play_button = QPushButton("Play")

            self.video_play_buttons[encrypted_path] = play_button  # Store play button
            play_button.clicked.connect(lambda: self.play_video_for_path(encrypted_path))


            # Set the new media instance to the video widget
            #self.media_player.set_media(media)

            # Add play and stop buttons for video playback
            layout = self.centralWidget().layout()
            layout.addSpacerItem(QSpacerItem(30, 30))

            play_button.clicked.connect(self.play_video)

            self.timeline_slider = QSlider(Qt.Horizontal)
            self.timeline_slider.setMinimum(0)
            self.timeline_slider.setMaximum(1000)
            self.timeline_slider.sliderMoved.connect(self.update_timeline)


            stop_button = QPushButton("Stop")
            stop_button.clicked.connect(self.stop_video)

            layout = self.centralWidget().layout()
            layout.addSpacerItem(QSpacerItem(30, 30))

            layout.addWidget(play_button)
            layout.addWidget(self.timeline_slider)
            layout.addWidget(stop_button)

            encrypted_filename = os.path.basename(encrypted_path)  # Get the encrypted filename
            self.restore_button.clicked.connect(lambda: self.restore_video(decrypted_data, encrypted_filename, restore_dir))

    def play_video_for_path(self, encrypted_path):
        if encrypted_path in self.video_media_instances:
            media = self.video_media_instances[encrypted_path]
            self.media_player.set_media(media)
            self.media_player.play()


    def decrypt_files(self, target_dir, restore_dir):
        password, ok = QInputDialog.getText(self, "Password", "Enter password:", QLineEdit.Password)
        if ok:
            for root, _, files in os.walk(target_dir):
                for file in files:
                    encrypted_path = os.path.join(root, file)
                    self.decrypt_and_display(encrypted_path, password, restore_dir)

    def decrypt_photos(self):
        self.decrypt_files(image_dir, self.restore_dir)

    def decrypt_videos(self):
        self.decrypt_files(video_dir, self.restore_dir)

    def __init__(self):
        super().__init__()


                # Create dictionaries to store media instances and play buttons for videos
        self.video_media_instances = {}
        self.video_play_buttons = {}

        self.setWindowTitle("Encrypted Text Editor")

        self.video_widget = QVideoWidget(self)
        self.video_widget.setGeometry(10, 320, 400, 300)

        self.text_edit = QTextEdit(self)
        self.text_edit.setGeometry(10, 10, 400, 300)

        self.drop_label = QLabel(self)
        self.drop_label.setGeometry(10, 320, 400, 300)

        self.image_size_slider = QSlider(Qt.Horizontal)
        self.image_size_slider.setMinimum(10)
        self.image_size_slider.setMaximum(100)
        self.image_size_slider.setValue(20)
        self.image_size_slider.valueChanged.connect(self.update_image_size)

        self.prev_button = QPushButton("Previous")
        self.next_button = QPushButton("Next")
        self.prev_button.clicked.connect(self.show_previous_image)
        self.next_button.clicked.connect(self.show_next_image)


        self.setAcceptDrops(True)
        self.image_paths = []
        self.current_image_index = -1
        self.video_paths = []

        self.vlc_instance = vlc.Instance()
        self.media_player = self.vlc_instance.media_player_new()
        self.media_player.set_fullscreen(False)

        self.encrypt_image_button = QPushButton("Encrypt and Save Image")
        self.encrypt_video_button = QPushButton("Encrypt and Save Video")
        self.restore_button = QPushButton("Restore")
        self.restore_button.clicked.connect(self.choose_restore_directory)



        self.encrypt_image_button.clicked.connect(self.encrypt_and_save_image)
        self.encrypt_video_button.clicked.connect(self.encrypt_and_save_video)

        self.decrypt_photo_button = QPushButton("Decrypt Photos")
        self.decrypt_video_button = QPushButton("Decrypt Videos")
        self.decrypt_photo_button.clicked.connect(self.decrypt_photos)
        self.decrypt_video_button.clicked.connect(self.decrypt_videos)



        layout = QVBoxLayout()
        layout.addWidget(self.text_edit)
        layout.addWidget(self.drop_label)
        layout.addWidget(self.image_size_slider)
        layout.addWidget(self.prev_button)
        layout.addWidget(self.next_button)
        layout.addWidget(self.encrypt_image_button)
        layout.addWidget(self.encrypt_video_button)
        layout.addWidget(self.restore_button)
        layout.addWidget(self.decrypt_photo_button)
        layout.addWidget(self.decrypt_video_button)




        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)


        self.setAcceptDrops(True)
        self.image_paths = []
        self.current_image_index = -1

    def encrypt_and_save_image(self):
        if self.image_paths:
            password, ok = QInputDialog.getText(self, "Password", "Enter password:", QLineEdit.Password)
            if ok:
                current_datetime = datetime.now().strftime('%d-%m-%Y-%H-%M-%S')
                target_dir = os.path.join(image_dir, current_datetime)
                os.makedirs(target_dir, exist_ok=True)
                for image_path in self.image_paths:
                    self.encrypt_and_save(image_path, password, is_video=False, target_dir=target_dir)
                QMessageBox.information(self, "Encryption", "All images encrypted and saved.")

    def encrypt_and_save_video(self):
        if self.media_player.get_media():
            password, ok = QInputDialog.getText(self, "Password", "Enter password:", QLineEdit.Password)
            if ok:
                current_datetime = datetime.now().strftime('%d-%m-%Y-%H-%M-%S')
                target_dir = os.path.join(video_dir, current_datetime)
                os.makedirs(target_dir, exist_ok=True)
                for video_path in self.video_paths:
                    self.encrypt_and_save(video_path, password, is_video=True, target_dir=target_dir)
                QMessageBox.information(self, "Encryption", "All videos encrypted and saved.")



    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def dropEvent(self, event):
        for url in event.mimeData().urls():
            file_path = url.toLocalFile()
            if file_path.lower().endswith(('.jpg', '.jpeg', '.png')):
                self.image_paths.append(file_path)
                if self.current_image_index == -1:
                    self.current_image_index = 0
                    self.update_image_size()
            elif file_path.lower().endswith(('.mp4', '.avi')):
                self.video_paths.append(file_path)
                if not self.media_player.get_media():
                    self.play_next_video()
                self.media_player.stop()
                self.video_widget.hide()

                media = self.vlc_instance.media_new(file_path)
                self.media_player.set_media(media)

                play_button = QPushButton("Play")
                play_button.clicked.connect(self.play_video)
                layout = self.centralWidget().layout()

                layout.addSpacerItem(QSpacerItem(30, 30))
                layout.addWidget(play_button)

                self.timeline_slider = QSlider(Qt.Horizontal)
                self.timeline_slider.setMinimum(0)
                self.timeline_slider.setMaximum(1000)
                self.timeline_slider.sliderMoved.connect(self.update_timeline)
                layout.addWidget(self.timeline_slider)


                self.stop_button = QPushButton("Stop")
                self.stop_button.clicked.connect(self.stop_video)

                layout = self.centralWidget().layout()
                layout.addWidget(self.stop_button)

                self.drop_label.show()

    def update_image_size(self):
        if self.image_paths:
            self.current_image_index %= len(self.image_paths)
            file_path = self.image_paths[self.current_image_index]
            if file_path.lower().endswith(('.jpg', '.jpeg', '.png')):
                pixmap = QPixmap(file_path)
                original_size = pixmap.size()
                scaled_size = original_size * self.image_size_slider.value() / 100
                scaled_pixmap = pixmap.scaled(scaled_size, Qt.KeepAspectRatio, Qt.SmoothTransformation)
                self.drop_label.setPixmap(scaled_pixmap)
                self.video_widget.hide()
                self.drop_label.show()
            elif file_path.lower().endswith(('.mp4', '.avi')):
                self.media_player.stop()
                self.drop_label.hide()
                self.video_widget.show()

    def play_video(self):
        self.media_player.play()
        #self.stop_button.show()
        self.timeline_slider.show()
        self.drop_label.hide()
        self.video_widget.show()

    def play_next_video(self):
        if self.video_paths:
            self.media_player.stop()
            video_path = self.video_paths[0]   # Get and remove the first video path
            media = self.vlc_instance.media_new(video_path)
            self.media_player.set_media(media)
            self.media_player.play()

    def stop_video(self):
        self.media_player.stop()
        #self.stop_button.hide()
        self.timeline_slider.hide()# Hide the stop button
        self.drop_label.show()
        self.video_widget.hide()
        #layout.addSpacerItem(QSpacerItem(30, 30))

    def update_timeline(self, position):
        self.media_player.set_position(position / 1000.0)
        self.media_player.set_position(desired_position)

    def show_previous_image(self):
        if self.image_paths:
            self.current_image_index -= 1
            self.update_image_size()

    def show_next_image(self):
        if self.image_paths:
            self.current_image_index += 1
            self.update_image_size()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = TextEditor()
    window.setGeometry(100, 100, 420, 800)
    window.show()
    sys.exit(app.exec_())
